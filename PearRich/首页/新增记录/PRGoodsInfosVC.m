//
//  PRGoodsInfosVC.m
//  PearRich
//
//  Created by apple on 2020/12/24.
//  Copyright © 2020 谢黎鹏. All rights reserved.
//

#import "PRGoodsInfosVC.h"
#import "GoodsNameMgrModel.h"
@interface PRGoodsInfosVC ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,retain)NSMutableArray * dataArr;
@property(nonatomic,retain)NSArray * originArr;
@property(nonatomic,retain)UITableView * myTab;
@property(nonatomic,assign)BOOL isEmptyTab;
@end

@implementation PRGoodsInfosVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.isHideNavBar = YES;
    self.isHideBottomLine = YES;
    self.dataArr = [NSMutableArray new];
    self.view.backgroundColor = UIColorYellow;
    
    self.myTab = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    self.myTab.delegate = self;
    self.myTab.dataSource = self;
    self.myTab.rowHeight = UITableViewAutomaticDimension;
    self.myTab.estimatedRowHeight = 45;
    self.myTab.backgroundColor = UnderLineColor;
    [self.view addSubview:self.myTab];
    self.myTab.sd_layout.spaceToSuperView(UIEdgeInsetsZero);
    
    [self getData];
}
-(void)getData{
    self.originArr = [GoodsNameMgrModel bg_findAll:GoodsNameTab];
    self.isEmptyTab = self.originArr.count == 0;
    if (self.originArr.count > 0) {
        [self.dataArr addObjectsFromArray:self.originArr];
        [self.myTab reloadData];
    }
}
-(void)filterInput:(NSString*)content{
    [self.dataArr removeAllObjects];
    if (content.length > 0) {
        for (GoodsNameMgrModel * mo in self.originArr) {
            if ([mo.name containsString:content]) {
                [self.dataArr addObject:mo];
            }
        }
    }else{
        [self.dataArr addObjectsFromArray:[GoodsNameMgrModel bg_findAll:GoodsNameTab]];
    }
    [self.myTab reloadData];
}
#pragma mark 代理方法
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArr.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        cell.textLabel.font = UIFontMake(AUTO(15));
        cell.contentView.backgroundColor = UnderLineColor;
    }
    if (indexPath.row < self.dataArr.count) {
        GoodsNameMgrModel * mo = self.dataArr[indexPath.row];
        cell.textLabel.text = mo.name;
    }
    return cell;
}
- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewRowAction *topRowAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:@"删除" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        GoodsNameMgrModel * mo = self.dataArr[indexPath.row];
        NSString* where = [NSString stringWithFormat:@"where %@=%@",bg_sqlKey(@"name"),bg_sqlValue(mo.name)];
        [GoodsNameMgrModel bg_delete:GoodsNameTab where:where];
        [self.dataArr removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath,nil] withRowAnimation:UITableViewRowAnimationNone];
    }];
    topRowAction.backgroundEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    return @[topRowAction];
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.01;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return nil;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return nil;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}
@end
