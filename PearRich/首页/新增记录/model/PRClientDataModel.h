//
//  PRClientDataModel.h
//  PearRich
//
//  Created by apple on 2020/12/24.
//  Copyright © 2020 谢黎鹏. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BGFMDB/BGFMDB.h>
NS_ASSUME_NONNULL_BEGIN

@interface PRClientDataModel : NSObject
@property(nonatomic,copy)NSString * name;
@property(nonatomic,copy)NSString * phone;
@end

NS_ASSUME_NONNULL_END
