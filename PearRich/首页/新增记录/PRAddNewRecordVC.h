//
//  PRAddNewRecordVC.h
//  PearRich
//
//  Created by 谢黎鹏 on 2020/1/12.
//  Copyright © 2020年 谢黎鹏. All rights reserved.
//

#import "SuperViewController.h"
#import "PRMainViewModel.h"
NS_ASSUME_NONNULL_BEGIN
typedef void(^SaveSuccessBlock)(void);
@interface PRAddNewRecordVC : SuperViewController
@property(nonatomic,retain)PRMainViewModel * editModel;
@property(nonatomic,copy)SaveSuccessBlock saveBlock;
@end

NS_ASSUME_NONNULL_END
