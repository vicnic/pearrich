//
//  PRClientNameTableView.h
//  PearRich
//
//  Created by apple on 2020/12/24.
//  Copyright © 2020 谢黎鹏. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class PRClientDataModel;
@interface PRClientNameTableView : UIView
@property(nonatomic,assign,readonly)BOOL isEmptyTab;
@property(nonatomic,copy)void(^choseNameClick)(PRClientDataModel * mo);
-(void)filterInput:(NSString*)content;
@end

NS_ASSUME_NONNULL_END
