//
//  PRClientNameTableView.m
//  PearRich
//
//  Created by apple on 2020/12/24.
//  Copyright © 2020 谢黎鹏. All rights reserved.
//

#import "PRClientNameTableView.h"
#import "PRClientDataModel.h"
@interface PRClientNameTableView()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,retain)UITableView * myTab;
@property(nonatomic,retain)NSArray * originArr;
@property(nonatomic,retain)NSMutableArray * dataArr;
@property(nonatomic,assign)BOOL isEmptyTab;
@end
@implementation PRClientNameTableView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.cornerRadius = 6;
        self.dataArr = [NSMutableArray new];
        self.myTab = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        self.myTab.delegate = self;
        self.myTab.dataSource = self;
        self.myTab.rowHeight = 45;
        self.myTab.backgroundColor = UnderLineColor;
        [self addSubview:self.myTab];
        self.myTab.sd_layout.spaceToSuperView(UIEdgeInsetsZero);
        [self getData];
    }
    return self;
}
-(void)getData{
    self.originArr = [PRClientDataModel bg_findAll:ClientNameTab];
    self.isEmptyTab = self.originArr.count == 0;
    if (self.originArr.count > 0) {
        [self.dataArr addObjectsFromArray:self.originArr];
        [self.myTab reloadData];
    }
}
-(void)filterInput:(NSString*)content{
    [self.dataArr removeAllObjects];
    for (PRClientDataModel * mo in self.originArr) {
        if ([mo.name containsString:content]) {
            [self.dataArr addObject:mo];
        }
    }
    NSInteger count = self.dataArr.count <=5 ?self.dataArr.count :5;
    CGFloat hei = count >5?(count * 45+20):(count * 45);
    if (hei > 80) {
        [UIView animateWithDuration:0.35 animations:^{
            self.height = hei;
        }];
    }
    
    [self.myTab reloadData];
}
#pragma mark 代理方法
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArr.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        cell.textLabel.font = UIFontMake(AUTO(15));
        cell.contentView.backgroundColor = UnderLineColor;
    }
    if (indexPath.row < self.dataArr.count) {
        PRClientDataModel * mo = self.dataArr[indexPath.row];
        cell.textLabel.text = mo.name;
    }
    return cell;
}
- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewRowAction *topRowAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:@"删除" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        PRClientDataModel * mo = self.dataArr[indexPath.row];
        NSString* where = [NSString stringWithFormat:@"where %@=%@",bg_sqlKey(@"name"),bg_sqlValue(mo.name)];
        [PRClientDataModel bg_delete:ClientNameTab where:where];
        [self.dataArr removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath,nil] withRowAnimation:UITableViewRowAnimationNone];
    }];
    topRowAction.backgroundEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    return @[topRowAction];
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.01;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return nil;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return nil;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row < self.dataArr.count) {
        PRClientDataModel * mo = self.dataArr[indexPath.row];
        if (self.choseNameClick) {
            self.choseNameClick(mo);
        }
    }    
}

@end
