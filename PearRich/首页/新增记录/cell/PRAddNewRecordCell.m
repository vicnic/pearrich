//
//  PRAddNewRecordCell.m
//  PearRich
//
//  Created by Jinniu on 2020/3/4.
//  Copyright © 2020 谢黎鹏. All rights reserved.
//

#import "PRAddNewRecordCell.h"
#import "PRGoodsInfosVC.h"
#import "GoodsNameMgrModel.h"
@interface PRAddNewRecordCell()<UITextFieldDelegate,QMUIKeyboardManagerDelegate>
@property(nonatomic,retain)UITextField * nameTF, *countTF, * inpriceTF, * saleTF;
@property(nonatomic,retain)QMUIKeyboardManager * keyboardMgr;
@property(nonatomic,assign)CGFloat keyboardHeight;
@property(nonatomic,retain)PRGoodsInfosVC * goodsVC;
@property(nonatomic,assign)BOOL waitKeyBoardHeight;
@end
@implementation PRAddNewRecordCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.keyboardMgr = [[QMUIKeyboardManager alloc]initWithDelegate:self];
        [self createCusUI];
    }
    return self;
}
-(void)createCusUI{
    self.nameTF = [[UITextField alloc]initWithFrame:Frame(0, 0, 120, 30)];
    _nameTF.font = [UIFont systemFontOfSize:14];
    _nameTF.placeholder = @"请输入名称";
    _nameTF.tag = 10 ;
    [_nameTF addTarget:self action:@selector(textFieldDidChangeValue:) forControlEvents:UIControlEventEditingChanged];
    _nameTF.delegate = self;
    _nameTF.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:self.nameTF];
    
    CGFloat w = (IPHONE_WIDTH-30-120)/3.f;
    self.countTF = [[UITextField alloc]initWithFrame:Frame(CGRectGetMaxX(self.nameTF.frame), 0, w, 30)];
    _countTF.font = [UIFont systemFontOfSize:14];
    _countTF.placeholder = @"请输入数量";
    _countTF.tag = 11 ;
    _countTF.keyboardType = UIKeyboardTypeNumberPad;
    [_countTF addTarget:self action:@selector(textFieldDidChangeValue:) forControlEvents:UIControlEventEditingChanged];
    _countTF.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:self.countTF];
    
    self.inpriceTF = [[UITextField alloc]initWithFrame:Frame(CGRectGetMaxX(self.countTF.frame), 0, w, 30)];
    _inpriceTF.font = [UIFont systemFontOfSize:14];
    _inpriceTF.placeholder = @"请输入进价";
    _inpriceTF.tag = 12 ;
    _inpriceTF.keyboardType = UIKeyboardTypeDecimalPad;
    [_inpriceTF addTarget:self action:@selector(textFieldDidChangeValue:) forControlEvents:UIControlEventEditingChanged];
    _inpriceTF.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:self.inpriceTF];
    
    self.saleTF = [[UITextField alloc]initWithFrame:Frame(CGRectGetMaxX(self.inpriceTF.frame), 0, w, 30)];
    _saleTF.font = [UIFont systemFontOfSize:14];
    _saleTF.placeholder = @"请输入售价";
    _saleTF.tag = 13 ;
    _saleTF.keyboardType = UIKeyboardTypeDecimalPad;
    [_saleTF addTarget:self action:@selector(textFieldDidChangeValue:) forControlEvents:UIControlEventEditingChanged];
    _saleTF.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:self.saleTF];
}
- (void)textFieldDidChangeValue:(UITextField*)textField{    
    switch (textField.tag) {
        case 10:{
            _model.goodsName = textField.text;
            if (!self.goodsVC.isEmptyTab) {
                [self.goodsVC filterInput:textField.text];
                if (self.goodsVC.view.x > SCREEN_WIDTH/2.0) {
                    [UIView animateWithDuration:0.35 animations:^{
                        self.goodsVC.view.x = SCREEN_WIDTH/2.0;
                    }];
                }                
            }
        }break;
        case 11:{
            _model.goodsCount = textField.text;
        }break;
        case 12:{
            _model.buyPrice = textField.text;
        }break;
        case 13:{
            _model.sellPrice = textField.text;
        }break;
        default:break;
    }
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField.tag == 10) {
        if (self.keyboardHeight == 0) {
            self.waitKeyBoardHeight = YES;
        }else{
            [self addGoodsViewView];
        }
    }
}
- (void)keyboardDidShowWithUserInfo:(QMUIKeyboardUserInfo *)keyboardUserInfo{
    self.keyboardHeight = keyboardUserInfo.height;
    if (_goodsVC) {
        [[Mytools currentViewController].view addSubview:self.goodsVC.view];
    }else{
        if (self.waitKeyBoardHeight) {
            [self addGoodsViewView];
            self.waitKeyBoardHeight = NO;
        }
    }
}
-(void)addGoodsViewView{
    [[Mytools currentViewController].view addSubview:self.goodsVC.view];
    [UIView animateWithDuration:0.35 animations:^{
        self.goodsVC.view.x = SCREEN_WIDTH/2.0;
    }];
}
- (void)setModel:(GoodsInfoModel *)model{
    _model = model;
    _nameTF.text = model.goodsName;
    _countTF.text = model.goodsCount;
    _inpriceTF.text = model.buyPrice;
    _saleTF.text = model.sellPrice;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    if (textField.text.length) {
        if (self.generBlock) {
            self.generBlock(YES);
        }
        NSString* where = [NSString stringWithFormat:@"where %@=%@",bg_sqlKey(@"name"),bg_sqlValue(textField.text)];
        NSArray* arr = [GoodsNameMgrModel bg_find:GoodsNameTab where:where];
        if (arr.count == 0) {
            GoodsNameMgrModel * mo = [GoodsNameMgrModel new];
            mo.name = textField.text;
            mo.bg_tableName = GoodsNameTab;
            [mo bg_saveAsync:^(BOOL isSuccess) {
                if (!isSuccess) {
                    [mo bg_save];
                }
            }];
        }
    }else{
        if (self.generBlock) {
            self.generBlock(NO);
        }
    }
    if (textField.tag == 10) {
        [UIView animateWithDuration:0.35 animations:^{
            self.goodsVC.view.x = SCREEN_WIDTH;
        }];
    }
}
- (PRGoodsInfosVC *)goodsVC{
    if (!_goodsVC) {
        _goodsVC = [PRGoodsInfosVC new];
        _goodsVC.view.cornerRadius = 6;
        _goodsVC.view.frame = Frame(SCREEN_WIDTH, kTopHeight+10, SCREEN_WIDTH/2.0, SCREEN_HEIGHT- KTabbarSafeBottomMargin-self.keyboardHeight-20-kTopHeight);
    }
    return _goodsVC;
}
@end
