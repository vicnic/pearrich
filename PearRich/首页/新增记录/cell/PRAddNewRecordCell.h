//
//  PRAddNewRecordCell.h
//  PearRich
//
//  Created by Jinniu on 2020/3/4.
//  Copyright © 2020 谢黎鹏. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PRMainViewModel.h"
NS_ASSUME_NONNULL_BEGIN
typedef void(^GenerateCellBlock)(BOOL isUp);
@interface PRAddNewRecordCell : UITableViewCell
@property(nonatomic,copy)GenerateCellBlock generBlock;
@property(nonatomic,retain)GoodsInfoModel * model;
@end

NS_ASSUME_NONNULL_END
