//
//  PRGoodsInfosVC.h
//  PearRich
//
//  Created by apple on 2020/12/24.
//  Copyright © 2020 谢黎鹏. All rights reserved.
//

#import "SuperViewController.h"

NS_ASSUME_NONNULL_BEGIN
@class GoodsNameMgrModel;
@interface PRGoodsInfosVC : SuperViewController
@property(nonatomic,assign,readonly)BOOL isEmptyTab;
-(void)filterInput:(NSString*)content;
@property(nonatomic,copy)void(^choseNameClick)(GoodsNameMgrModel * mo);
@end

NS_ASSUME_NONNULL_END
