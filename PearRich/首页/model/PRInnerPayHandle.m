//
//  PRInnerPayHandle.m
//  PearRich
//
//  Created by apple on 2020/12/30.
//  Copyright © 2020 谢黎鹏. All rights reserved.
//

#import "PRInnerPayHandle.h"
#import "YQInAppPurchaseTool.h"
@interface PRInnerPayHandle()<YQInAppPurchaseToolDelegate>
@property(nonatomic,retain)YQInAppPurchaseTool * iapTool;
@end
@implementation PRInnerPayHandle
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.iapTool = [YQInAppPurchaseTool defaultTool];
        self.iapTool.delegate = self;
        [self.iapTool requestProductsWithProductArray:@[@"li_vip"]];
    }
    return self;
}
-(void)IAPToolGotProducts:(NSMutableArray *)products {
    NSLog(@"GotProducts:%@",products);
    if(products.count > 0){
        [self.iapTool buyProduct:@"li_vip"];
    }
}
//支付失败/取消
-(void)IAPToolCanceldWithProductID:(NSString *)productID {
    NSLog(@"canceld:%@",productID);
}
//支付成功了，并开始向苹果服务器进行验证（若CheckAfterPay为NO，则不会经过此步骤）
-(void)IAPToolBeginCheckingdWithProductID:(NSString *)productID {
    NSLog(@"BeginChecking:%@",productID);
}
//商品被重复验证了
-(void)IAPToolCheckRedundantWithProductID:(NSString *)productID {
    NSLog(@"CheckRedundant:%@",productID);
}
//商品完全购买成功且验证成功了。（若CheckAfterPay为NO，则会在购买成功后直接触发此方法）
-(void)IAPToolBoughtProductSuccessedWithProductID:(NSString *)productID
                                            andInfo:(NSDictionary *)infoDic {
    NSLog(@"BoughtSuccessed:%@",productID);
    NSLog(@"successedInfo:%@",infoDic);
}
//商品购买成功了，但向苹果服务器验证失败了
//2种可能：
//1，设备越狱了，使用了插件，在虚假购买。
//2，验证的时候网络突然中断了。（一般极少出现，因为购买的时候是需要网络的）
-(void)IAPToolCheckFailedWithProductID:(NSString *)productID
                                 andInfo:(NSData *)infoData {
    NSLog(@"CheckFailed:%@",productID);
}
//恢复了已购买的商品（仅限永久有效商品）
-(void)IAPToolRestoredProductID:(NSString *)productID {
    NSLog(@"Restored:%@",productID);
}
//内购系统错误了
-(void)IAPToolSysWrong {
    NSLog(@"SysWrong");
}
@end
