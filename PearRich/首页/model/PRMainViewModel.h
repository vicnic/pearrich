//
//  PRMainViewModel.h
//  PearRich
//
//  Created by 谢黎鹏 on 2020/1/11.
//  Copyright © 2020年 谢黎鹏. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BGFMDB/BGFMDB.h>
NS_ASSUME_NONNULL_BEGIN
@class GoodsInfoModel;
@interface PRMainViewModel : NSObject
@property(nonatomic,copy)NSString * person,*phone,*desc,*createTime,* modifyTime,* isSend,*guestIsPay,* remark,*mailFee;
@property (nonatomic,retain)NSMutableArray <GoodsInfoModel*>* goodsInfoArr;
@end
@interface GoodsInfoModel :NSObject
@property(nonatomic,copy)NSString * goodsName ,* goodsCount,* buyPrice, * sellPrice;
@end

NS_ASSUME_NONNULL_END
