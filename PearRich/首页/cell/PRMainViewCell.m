//
//  PRMainViewCell.m
//  PearRich
//
//  Created by 谢黎鹏 on 2020/1/11.
//  Copyright © 2020年 谢黎鹏. All rights reserved.
//

#import "PRMainViewCell.h"
@interface PRMainViewCell()
@property (weak, nonatomic) IBOutlet UILabel *goodsNameLab;
@property (weak, nonatomic) IBOutlet UILabel *nameLab;
@property (weak, nonatomic) IBOutlet UILabel *phoneLab;
@property (weak, nonatomic) IBOutlet UILabel *descLab;
@property (weak, nonatomic) IBOutlet UIView *sendStatueMarkView;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UILabel *sendStateLab;
@property (weak, nonatomic) IBOutlet UIView *lineView;
@property (weak, nonatomic) IBOutlet UIView *bgView;

@end
@implementation PRMainViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.bgView.backgroundColor = [UIColor normalColor:UIColorWhite darkColor:[UIColor darkGrayColor]];
    self.lineView.backgroundColor = UnderLineColor;
}
- (void)setModel:(PRMainViewModel *)model{
    self.nameLab.text = model.person;
    self.phoneLab.text = model.phone;
    self.descLab.text = model.desc;
    self.sendStateLab.text = model.isSend;
    if ([model.isSend isEqualToString:@"未发货"]) {
        self.sendStateLab.textColor = [UIColor redColor];
        self.sendStatueMarkView.backgroundColor = [UIColor redColor];
    }else{
        self.sendStateLab.textColor = [UIColor greenColor];
        self.sendStatueMarkView.backgroundColor = [UIColor greenColor];
    }
    for (int i =0 ;i<model.goodsInfoArr.count; i ++) {
        GoodsInfoModel * mo = model.goodsInfoArr[i];
        if (i==0) {
            self.goodsNameLab.text = mo.goodsName;
        }else{
            if (mo.goodsName.length) {
                self.goodsNameLab.text = [NSString stringWithFormat:@"%@+%@",self.goodsNameLab.text,mo.goodsName];
            }
        }
    }
}
- (void)setIsLastRow:(BOOL)isLastRow{
    if (isLastRow) {
        self.lineView.hidden = YES;
    }else{
        self.lineView.hidden = NO;
    }
}@end
