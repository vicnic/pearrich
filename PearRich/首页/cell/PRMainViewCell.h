//
//  PRMainViewCell.h
//  PearRich
//
//  Created by 谢黎鹏 on 2020/1/11.
//  Copyright © 2020年 谢黎鹏. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PRMainViewModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface PRMainViewCell : UITableViewCell
@property(nonatomic,retain)PRMainViewModel * model;
@property(nonatomic,assign)BOOL isLastRow;
@end

NS_ASSUME_NONNULL_END
