//
//  main.m
//  PearRich
//
//  Created by 谢黎鹏 on 2020/1/11.
//  Copyright © 2020年 谢黎鹏. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
