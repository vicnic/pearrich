//
//  SearchRecordCell.m
//  PearRich
//
//  Created by Jinniu on 2020/3/13.
//  Copyright © 2020 谢黎鹏. All rights reserved.
//

#import "SearchRecordCell.h"
@interface SearchRecordCell()
@property(nonatomic,retain)UILabel * nameLab;
@end
@implementation SearchRecordCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self createCusUI];
    }
    return self;
}
-(void)createCusUI{
    UIImageView * img = [UIImageView new];
    img.image = Image(@"search_history");
    [self.contentView addSubview:img];
    img.sd_layout.leftSpaceToView(self.contentView, 15).centerYEqualToView(self.contentView).heightIs(16).widthEqualToHeight();
    
    self.nameLab = [UILabel labelWithTitle:@"" color:[UIColor blackColor] fontSize:14];
    [self.contentView addSubview:self.nameLab];
    _nameLab.sd_layout.leftSpaceToView(img, 15).centerYEqualToView(img).heightIs(20);
    [_nameLab setSingleLineAutoResizeWithMaxWidth: 300];
    
    UIButton * closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [closeBtn setImage:Image(@"close") forState:UIControlStateNormal];
    [closeBtn addTarget:self action:@selector(closeBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:closeBtn];
    closeBtn.sd_layout.rightSpaceToView(self.contentView, 15).centerYEqualToView(self.contentView).heightIs(40).widthEqualToHeight();
}
- (void)setModel:(SearchRecordModel *)model{
    self.nameLab.text = model.name;
}
-(void)closeBtnClick{
    if (self.delBlock) {
        self.delBlock();
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
