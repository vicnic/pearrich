//
//  SearchRecordCell.h
//  PearRich
//
//  Created by Jinniu on 2020/3/13.
//  Copyright © 2020 谢黎鹏. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchRecordModel.h"
NS_ASSUME_NONNULL_BEGIN
typedef void(^DeleteBlock)(void);
@interface SearchRecordCell : UITableViewCell
@property(nonatomic,retain)SearchRecordModel * model;
@property(nonatomic,copy)DeleteBlock delBlock;
@end

NS_ASSUME_NONNULL_END
