//
//  SearchRecordModel.h
//  PearRich
//
//  Created by Jinniu on 2020/3/13.
//  Copyright © 2020 谢黎鹏. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BGFMDB/BGFMDB.h>
NS_ASSUME_NONNULL_BEGIN

@interface SearchRecordModel : NSObject
@property(nonatomic,copy)NSString * name;
@end

NS_ASSUME_NONNULL_END
