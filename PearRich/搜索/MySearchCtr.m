//
//  MySearchCtr.m
//  PearRich
//
//  Created by Jinniu on 2020/3/13.
//  Copyright © 2020 谢黎鹏. All rights reserved.
//

#import "MySearchCtr.h"
#import "SearchRecordView.h"
#import "HXSearchBar.h"
#import "PRAddNewRecordVC.h"
#import "PRMainViewCell.h"
#import "SearchRecordModel.h"

@interface MySearchCtr ()<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>{
    NSString * _searchText;
}
@property(nonatomic,retain)UITableView * myTab;
@property(nonatomic,retain)HXSearchBar * searchTF;
@property(nonatomic,retain)SearchRecordView * sRecordView;
@property(nonatomic,retain)NSMutableArray * dataArr;
@end

@implementation MySearchCtr

- (void)viewDidLoad {
    [super viewDidLoad];
    self.myTitle = @"搜索";
    self.dataArr = [NSMutableArray new];
    [self crateTable];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [Mytools NeedResetNoViewWithTable:self.myTab andArr:self.dataArr];
}
-(void)crateTable{
    self.myTab = [[UITableView alloc]initWithFrame:Frame(0, kTopHeight, IPHONE_WIDTH, IPHONE_HEIGHT-kTopHeight-kTabbarHeight) style:UITableViewStyleGrouped];
    self.myTab.delegate = self;
    self.myTab.dataSource = self;
    self.myTab.estimatedRowHeight = 100;
    self.myTab.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.myTab registerNib:[UINib nibWithNibName:@"PRMainViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    [self.view addSubview:self.myTab];
}
-(void)searchBtnClick{
    if (self.sRecordView) {
        return;
    }
    self.sRecordView = [[SearchRecordView alloc]initWithFrame:Frame(0, kStatusBarHeight + 45, IPHONE_WIDTH, IPHONE_HEIGHT-kStatusBarHeight-45-kTabbarHeight)];
    self.sRecordView.alpha = 0;
    MJWeakSelf;
    self.sRecordView.selBlock = ^(NSString * _Nonnull keyWord) {
        [weakSelf.dataArr removeAllObjects];
        self->_searchText = keyWord;
        [weakSelf searchSQL];
    };
    [self.view addSubview:self.sRecordView];
    self.isHideBottomLine = YES;
    [UIView animateWithDuration:0.35 animations:^{
        self.navView.alpha = 0;
        self.navView.height = kStatusBarHeight;
        self.myTab.y = kStatusBarHeight;
        self.sRecordView.alpha = 1;
    }];
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self.dataArr removeAllObjects];
    _searchText = searchBar.text;
    [self searchSQL];
    
    SearchRecordModel * mo = [SearchRecordModel new];
    mo.name = _searchText;
    mo.bg_tableName = SearchRecordTab;
    [mo bg_saveAsync:^(BOOL isSuccess) {
        if (!isSuccess) {
            [mo bg_save];
        }
    }];
}
-(void)searchSQL{
    NSString *sql=[NSString stringWithFormat:@"SELECT * FROM GoodsInfoTab WHERE BG_person LIKE '%%%@%%' OR BG_phone LIKE '%%%@%%' OR BG_remark LIKE '%%%@%%'",_searchText,_searchText,_searchText];
    NSArray * arr = bg_executeSql(sql, GoodsInfoTab, [PRMainViewModel class]);
    [_dataArr addObjectsFromArray:arr];
    NSString * goodsSQL = [NSString stringWithFormat:@"SELECT * FROM GoodsInfoTab WHERE BG_goodsInfoArr LIKE '%%%@%%'",_searchText];
    NSArray * goodsArr = bg_executeSql(goodsSQL, GoodsInfoTab, [GoodsInfoModel class]);
    for (GoodsInfoModel * g in goodsArr) {
        NSString* where = [NSString stringWithFormat:@"where %@=%@",bg_sqlKey(bg_primaryKey),g.bg_id];
        NSArray* arr = [PRMainViewModel bg_find:GoodsInfoTab where:where];
        [_dataArr addObjectsFromArray:arr];
    }
    [_myTab reloadData];
    [self dismissSRecordView];
    [Mytools NeedResetNoViewWithTable:self.myTab andArr:self.dataArr];
}
-(HXSearchBar*)searchTF{
    if (_searchTF == nil) {
        _searchTF = [[HXSearchBar alloc]initWithFrame:Frame(7.5, 5, IPHONE_WIDTH-15, 35)];
        _searchTF.backgroundColor = [UIColor normalColor:UIColorWhite darkColor:UIColorBlack];
        _searchTF.delegate = self;
        _searchTF.cornerRadius = 4;
        _searchTF.placeholder = @"请输入搜索内容";
        //光标颜色
        _searchTF.cursorColor = [UIColor lightGrayColor];
        _searchTF.clearButtonImage = [UIImage imageNamed:@"search_delete"];
        //去掉取消按钮灰色背景
        _searchTF.showsCancelButton = YES;
        _searchTF.hideSearchBarBackgroundImage = YES;
    }
    return _searchTF;
}
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    [self searchBtnClick];
    return YES;
}
//取消按钮点击的回调
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [self dismissSRecordView];
}
-(void)dismissSRecordView{
    [self.searchTF endEditing:YES];
    self.searchTF.text= @"";
    [UIView animateWithDuration:0.35 animations:^{
        self.sRecordView.alpha = 0;
        self.navView.alpha = 1;
        self.navView.height = kTopHeight;
        self.myTab.y = kTopHeight;
    }completion:^(BOOL finished) {
        [self.sRecordView removeFromSuperview];
        self.sRecordView = nil;
        self.isHideBottomLine = YES;
    }];
}
- (void)keyboardWillHide:(NSNotification *)notification {
    [self dismissSRecordView];
}
#pragma mark tableView代理
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArr.count;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    PRMainViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.model = self.dataArr[indexPath.row];
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 45;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView * headView = [UIView new];
    [headView addSubview:self.searchTF];
    return headView;
}
-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return nil;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    PRMainViewModel * mo = self.dataArr[indexPath.row];
    PRAddNewRecordVC * vc = [PRAddNewRecordVC new];
    vc.editModel = mo;
    vc.saveBlock = ^{
        [self searchSQL];
        NSNotificationCenter * center = [NSNotificationCenter defaultCenter];
        NSNotification * message = [[NSNotification alloc]initWithName:@"mainviewreload" object:self userInfo:nil];
        [center postNotification:message];
    };
    [self.navigationController pushViewController:vc animated:YES];
}

@end
