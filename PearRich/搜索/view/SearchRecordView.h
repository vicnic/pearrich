//
//  SearchRecordView.h
//  PearRich
//
//  Created by Jinniu on 2020/3/13.
//  Copyright © 2020 谢黎鹏. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void(^TableSelectBlock)(NSString * keyWord);
@interface SearchRecordView : UIView
@property(nonatomic,copy)TableSelectBlock selBlock;
@end

NS_ASSUME_NONNULL_END
