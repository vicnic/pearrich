//
//  QRCodeInputTV.h
//  PearRich
//
//  Created by Jinniu on 2020/3/19.
//  Copyright © 2020 谢黎鹏. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void(^AniEndBlock)(NSString * str);
@interface QRCodeInputTV : UIView
@property(nonatomic,copy)AniEndBlock aniBlock;
@property(nonatomic,copy)NSString * inputContent;
@property(nonatomic,copy)NSString * certainBtnName;
@property(nonatomic,copy)NSString * certainAlertMsg;
@property(nonatomic,assign)BOOL certainClickIsCopy;//默认NO
@end

NS_ASSUME_NONNULL_END
