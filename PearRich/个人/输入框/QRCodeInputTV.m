//
//  QRCodeInputTV.m
//  PearRich
//
//  Created by Jinniu on 2020/3/19.
//  Copyright © 2020 谢黎鹏. All rights reserved.
//

#import "QRCodeInputTV.h"
@interface QRCodeInputTV()
@property(nonatomic,retain)UIButton * certainBtn;
@property(nonatomic,retain)QMUITextView * tv;
@end
@implementation QRCodeInputTV

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.cornerRadius = 5;
        self.backgroundColor = [UIColor normalColor:UIColorWhite darkColor:[UIColor darkGrayColor]];
        [self createUI];
    }
    return self;
}
-(void)createUI{
    self.tv = [QMUITextView new];
    _tv.placeholder = @"请输入内容";
    _tv.placeholderColor = [UIColor lightGrayColor];
    _tv.backgroundColor = [UIColor normalColor:UIColorWhite darkColor:[UIColor darkGrayColor]];
    _tv.font = [UIFont systemFontOfSize:14];
    [self addSubview:self.tv];
    self.tv.sd_layout.leftSpaceToView(self, 5).topSpaceToView(self, 5).rightSpaceToView(self, 5).heightIs(190);
    
    UIView * line = [UIView new];
    line.backgroundColor = [UIColor normalColor:LineColor darkColor:[UIColor lightGrayColor]];
    [self addSubview:line];
    line.sd_layout.leftEqualToView(self).topSpaceToView(_tv, 5).rightEqualToView(self).heightIs(0.7);
    
    UIButton * cancel = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancel setTitle:@"取消" forState:UIControlStateNormal];
    cancel.titleLabel.font = [UIFont systemFontOfSize:14];
    [cancel addTarget:self action:@selector(cancelBtnClick) forControlEvents:UIControlEventTouchUpInside];
    cancel.backgroundColor = [UIColor normalColor:UIColorWhite darkColor:[UIColor darkGrayColor]];
    [cancel setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [self addSubview:cancel];
    cancel.sd_layout.leftEqualToView(self).topSpaceToView(line, 0).bottomEqualToView(self).widthIs((IPHONE_WIDTH-30)/2.f);
    
    self.certainBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.certainBtn setTitleColor:ThemeColor forState:UIControlStateNormal];
    [self.certainBtn setTitle:@"确定" forState:UIControlStateNormal];
    self.certainBtn.backgroundColor = [UIColor normalColor:UIColorWhite darkColor:[UIColor darkGrayColor]];
    [self.certainBtn addTarget:self action:@selector(certainBtnClick) forControlEvents:UIControlEventTouchUpInside];
    self.certainBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [self addSubview:self.certainBtn];
    self.certainBtn.sd_layout.leftSpaceToView(cancel, 0).topSpaceToView(line, 0).bottomEqualToView(self).widthIs((IPHONE_WIDTH-30)/2.f);
    UIView * vline = [UIView new];
    vline.backgroundColor = [UIColor normalColor:LineColor darkColor:[UIColor lightGrayColor]];
    [self addSubview:vline];
    vline.sd_layout.topSpaceToView(line, 0).bottomSpaceToView(self, 0).widthIs(0.7).centerXEqualToView(self);
    
}
-(void)cancelBtnClick{
    self.transform = CGAffineTransformMakeScale(1.0, 1.0);
    [UIView animateWithDuration:0.35 animations:^{
        self.transform = CGAffineTransformMakeScale(0.01, 0.01);
    }completion:^(BOOL finished) {
        if (self.aniBlock) {
            self.aniBlock(@"");
        }
    }];
}
- (void)setInputContent:(NSString *)inputContent{
    _tv.text = inputContent;
}
- (void)setCertainBtnName:(NSString *)certainBtnName{
    _certainBtnName = certainBtnName;
    [self.certainBtn setTitle:certainBtnName forState:UIControlStateNormal];
}
- (void)setCertainAlertMsg:(NSString *)certainAlertMsg{
    _certainAlertMsg = certainAlertMsg;
}
- (void)setCertainClickIsCopy:(BOOL)certainClickIsCopy{
    _certainClickIsCopy = certainClickIsCopy;
}
-(void)certainBtnClick{
    if (self.certainAlertMsg.length) {
        [QMUITips showSucceed:self.certainAlertMsg];
    }
    if (self.certainClickIsCopy) {
        UIPasteboard * board = [UIPasteboard generalPasteboard];
        board.string = self.tv.text;
        if (self.aniBlock) {
            self.aniBlock(self.tv.text);
        }
    }else{
        self.transform = CGAffineTransformMakeScale(1.0, 1.0);
        [UIView animateWithDuration:0.35 animations:^{
            self.transform = CGAffineTransformMakeScale(0.01, 0.01);
        }completion:^(BOOL finished) {
            if (self.aniBlock) {
                self.aniBlock(self.tv.text);
            }
        }];
    }
    
}
@end
