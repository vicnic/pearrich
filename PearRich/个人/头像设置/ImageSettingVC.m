//
//  ImageSettingVC.m
//  PearRich
//
//  Created by Jinniu on 2020/3/25.
//  Copyright © 2020 谢黎鹏. All rights reserved.
//

#import "ImageSettingVC.h"
#import <PINCache/PINCache.h>
#import <TZImagePickerController/TZImagePickerController.h>

@interface ImageSettingVC ()<TZImagePickerControllerDelegate>
@property(nonatomic,retain)UISwitch * sw;
@property(nonatomic,retain)UIImageView * smallView;
@property(nonatomic,assign)BOOL hasQRLogo,hasChatLogo;
@end

@implementation ImageSettingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.myTitle = @"设置logo";
    self.view.backgroundColor = [UIColor normalColor:UnderLineColor darkColor:[UIColor blackColor]];
    PINCache * ca = [[PINCache alloc]initWithName:QrCodeLogoFilePath];
    self.hasQRLogo = [ca containsObjectForKey:QrCodeLogoFilePath];
    
    [self createUI];
}
-(void)createUI{
    NSArray * nameArr = @[@"设置二维码logo"];
    for (int i =0;i<nameArr.count;i++) {
        NSString * name = nameArr[i];
        UIView * v = [[UIView alloc]initWithFrame:Frame(0, kTopHeight + 10 + 50*i, IPHONE_WIDTH, 49)];
        v.backgroundColor = [UIColor normalColor:[UIColor whiteColor] darkColor:[UIColor darkGrayColor]];
        [self.view addSubview:v];
        UILabel * lab = [UILabel labelWithTitle:name color:[UIColor normalColor:[UIColor blackColor] darkColor:[UIColor whiteColor]] fontSize:14];
        [v addSubview:lab];
        lab.sd_layout.leftSpaceToView(v, 15).centerYEqualToView(v).heightIs(20);
        [lab setSingleLineAutoResizeWithMaxWidth:300];
        UIImageView * arrow = [UIImageView new];
        arrow.image = Image(@"右箭头");
        [v addSubview:arrow];
        arrow.sd_layout.rightSpaceToView(v, 10).centerYEqualToView(v).heightIs(16).widthEqualToHeight();
        if (i==0) {
            self.sw = [UISwitch new];
            self.sw.transform = CGAffineTransformMakeScale(0.75, 0.75);
            self.sw.onTintColor= ThemeColor;
            self.sw.on = self.hasQRLogo;
            [self.sw addTarget:self action:@selector(swAction) forControlEvents:UIControlEventValueChanged];
            [v addSubview:self.sw];
            _sw.sd_layout.rightSpaceToView(arrow, 5).centerYEqualToView(v).offset(5).heightIs(20).widthIs(30);
        }
    }
}
-(void)swAction{
    if ([[DLUserDefaultModel userDefaultsModel].isShowLogoSW isEqualToString:@"YES"]) {
        [DLUserDefaultModel userDefaultsModel].isShowLogoSW = @"NO";
    }else{
        if (self.hasQRLogo) {
            [DLUserDefaultModel userDefaultsModel].isShowLogoSW = @"YES";
            [self showAlert:@"检测到您再次打开此开关，是否重新选择图片？"];
        }else{
            [self showAlert:@"您尚未设置logo，是否前往设置"];
        }
    }
}
-(void)showAlert:(NSString*)title{
    QMUIAlertController *alertController = [QMUIAlertController alertControllerWithTitle:@"温馨提示" message:title preferredStyle:QMUIAlertControllerStyleAlert];
    [alertController addAction:[QMUIAlertAction actionWithTitle:@"确定" style:QMUIAlertActionStyleDestructive handler:^(__kindof QMUIAlertController * _Nonnull aAlertController, QMUIAlertAction * _Nonnull action) {
        [self choseImage:YES];
    }]];
    [alertController addAction:[QMUIAlertAction actionWithTitle:@"我手滑" style:QMUIAlertActionStyleCancel handler:^(__kindof QMUIAlertController * _Nonnull aAlertController, QMUIAlertAction * _Nonnull action) {
        self.sw.on = !self.sw.on;
    }]];
    [alertController showWithAnimated:YES];
    
}
-(void)choseImage:(BOOL)isSW{
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:1 delegate:self];
    imagePickerVc.naviBgColor = ThemeColor;
    imagePickerVc.allowPickingVideo = NO;
    imagePickerVc.scaleAspectFillCrop = YES;
    imagePickerVc.showSelectBtn = NO;
    [imagePickerVc setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
        UIImage * img = photos[0];
        PINCache * ca = [[PINCache alloc]initWithName:QrCodeLogoFilePath];
        [ca setObjectAsync:img forKey:QrCodeLogoFilePath completion:^(__kindof id<PINCaching>  _Nonnull cache, NSString * _Nonnull key, id  _Nullable object) {
            [DLUserDefaultModel userDefaultsModel].isShowLogoSW = @"YES";
            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                [QMUITips showSucceed:@"设置成功"];
                self.sw.on = YES;
            }];
        }];
    }];
    [imagePickerVc setImagePickerControllerDidCancelHandle:^{
        if (isSW) {
            self.sw.on = NO;
        }
    }];
    [self presentViewController:imagePickerVc animated:YES completion:nil];
}

@end
