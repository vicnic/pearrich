//
//  PersonCell.h
//  PearRich
//
//  Created by 谢黎鹏 on 2020/3/17.
//  Copyright © 2020年 谢黎鹏. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PersonCell : UITableViewCell
@property(nonatomic,copy)NSString * name, * imgName;
@property(nonatomic,retain)UIImageView * arrow;
@property(nonatomic,assign)BOOL firstRow, lastRow,isShowSw;
@property(nonatomic,assign)NSInteger swTag;
@end

NS_ASSUME_NONNULL_END
