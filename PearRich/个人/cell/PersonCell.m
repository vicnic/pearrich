//
//  PersonCell.m
//  PearRich
//
//  Created by 谢黎鹏 on 2020/3/17.
//  Copyright © 2020年 谢黎鹏. All rights reserved.
//

#import "PersonCell.h"
#import <PINCache/PINCache.h>
#import <TZImagePickerController/TZImagePickerController.h>
#import "ESUserFingerSetPswCtr.h"
static NSString* filePath = @"self.filePath";

@interface PersonCell()<TZImagePickerControllerDelegate>
@property(nonatomic,retain)UIImageView * iconView;
@property(nonatomic,retain)UILabel * nameLab;
@property(nonatomic,retain)UIView * bgView,*line;
@property(nonatomic,retain)UISwitch * sw;
@end
@implementation PersonCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self createCusUI];
    }
    return self;
}
- (void)setName:(NSString *)name{
    _nameLab.text = name;
}
- (void)setImgName:(NSString *)imgName{
    self.iconView.image = Image(imgName);
}
-(void)createCusUI{
    self.bgView = [UIView new];
    _bgView.backgroundColor = [UIColor normalColor:UIColorWhite darkColor:[UIColor darkGrayColor]];
    [self.contentView addSubview:self.bgView];
    _bgView.sd_layout.leftSpaceToView(self.contentView, 15).rightSpaceToView(self.contentView, 15).topEqualToView(self.contentView).bottomEqualToView(self.contentView);
    
    self.iconView = [UIImageView new];
    [self.bgView addSubview:self.iconView];
    _iconView.sd_layout.leftSpaceToView(self.bgView, 10).centerYEqualToView(self.bgView).heightIs(25).widthEqualToHeight();
    
    self.nameLab = [UILabel labelWithTitle:@"" color:[UIColor normalColor:UIColorBlack darkColor:UIColorWhite] fontSize:14];
    [self.bgView addSubview:self.nameLab];
    _nameLab.sd_layout.leftSpaceToView(self.iconView, 10).centerYEqualToView(self.iconView).heightIs(14);
    [_nameLab setSingleLineAutoResizeWithMaxWidth:300];
    
    self.arrow = [UIImageView new];
    self.arrow.image = Image(@"右箭头");
    [_bgView addSubview:self.arrow];
    self.arrow.sd_layout.rightSpaceToView(_bgView, 10).centerYEqualToView(_bgView).heightIs(16).widthEqualToHeight();
    
    self.sw = [UISwitch new];
    self.sw.transform = CGAffineTransformMakeScale(0.75, 0.75);
    self.sw.onTintColor= ThemeColor;
    self.sw.on = NO;
    [self.sw addTarget:self action:@selector(swAction) forControlEvents:UIControlEventValueChanged];
    [_bgView addSubview:self.sw];
    _sw.sd_layout.rightSpaceToView(_bgView, 5).centerYEqualToView(_bgView).offset(5).heightIs(20).widthIs(30);
    
    self.line = [UIView new];
    self.line.backgroundColor = LineColor;
    [_bgView addSubview:self.line];
    self.line.sd_layout.leftEqualToView(_bgView).rightEqualToView(_bgView).bottomEqualToView(_bgView).heightIs(0.7);
    
}
- (void)setFirstRow:(BOOL)firstRow{
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:Frame(0, 0, IPHONE_WIDTH-30, 45) byRoundingCorners:UIRectCornerTopLeft|UIRectCornerTopRight cornerRadii:CGSizeMake(5, 5)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.bgView.bounds;
    maskLayer.path = maskPath.CGPath;
    self.bgView.layer.mask = maskLayer;
}
- (void)setLastRow:(BOOL)lastRow{
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:Frame(0, 0, IPHONE_WIDTH-30, 45) byRoundingCorners:UIRectCornerBottomLeft|UIRectCornerBottomRight cornerRadii:CGSizeMake(5, 5)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.bgView.bounds;
    maskLayer.path = maskPath.CGPath;
    self.bgView.layer.mask = maskLayer;
    
    self.line.hidden = YES;
}
- (void)setIsShowSw:(BOOL)isShowSw{
    _isShowSw = isShowSw;
    if (isShowSw) {
        self.arrow.hidden = YES;
        self.sw.hidden = NO;
    }else{
        self.arrow.hidden = NO;
        self.sw.hidden = YES;
    }
}
- (void)setSwTag:(NSInteger)swTag{
    _swTag = swTag;
    if (swTag==1){
        self.arrow.hidden = YES;
        if (iPhoneX) {
            if ([[DLUserDefaultModel userDefaultsModel].faceIdentify isEqualToString:@"YES"]) {
                self.sw.on = YES;
            }else{
                self.sw.on = NO;
            }
        }else{
            if ([[DLUserDefaultModel userDefaultsModel].thumbIdentify isEqualToString:@"YES"]) {
                self.sw.on = YES;
            }else{
                self.sw.on = NO;
            }
        }
    }else if (swTag == 2){
        if ([[DLUserDefaultModel userDefaultsModel].gestureIdentify isEqualToString:@"YES"]) {
            self.sw.on = YES;
        }else{
            self.sw.on = NO;
        }
    }
}
-(void)swAction{
    if (_swTag == 1){
        if (iPhoneX) {
            if ([[DLUserDefaultModel userDefaultsModel].faceIdentify isEqualToString:@"YES"]) {
                [DLUserDefaultModel userDefaultsModel].faceIdentify = @"NO";
            }else{
                [DLUserDefaultModel userDefaultsModel].faceIdentify = @"YES";
            }
        }else{
            if ([[DLUserDefaultModel userDefaultsModel].thumbIdentify isEqualToString:@"YES"]) {
                [DLUserDefaultModel userDefaultsModel].thumbIdentify = @"NO";
            }else{
                [DLUserDefaultModel userDefaultsModel].thumbIdentify = @"YES";
            }
        }
    }else if (_swTag == 2){
        if ([[DLUserDefaultModel userDefaultsModel].gestureIdentify isEqualToString:@"YES"]) {
            [DLUserDefaultModel userDefaultsModel].gestureIdentify = @"NO";
        }else{
            ESUserFingerSetPswCtr * vc = [ESUserFingerSetPswCtr new];
            vc.gestureType = 0;
            vc.gesBackBlock = ^(NSInteger type) {
                if (type==1) {
                    [DLUserDefaultModel userDefaultsModel].gestureIdentify = @"YES";
                }else{
                    [DLUserDefaultModel userDefaultsModel].gestureIdentify = @"NO";
                    [self.sw setOn:NO];
                }
            };
            [[Mytools currentViewController].navigationController pushViewController:vc animated:YES];            
        }
    }
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
