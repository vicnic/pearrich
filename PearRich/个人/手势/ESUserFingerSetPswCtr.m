//
//  ESUserFingerSetPswCtr.m
//  ElectricSilverDelegate
//
//  Created by Jinniu on 2017/9/22.
//  Copyright © 2017年 zou. All rights reserved.
//

#import "ESUserFingerSetPswCtr.h"
#import "PCCircleView.h"
#import "PCLockLabel.h"
#import "PCCircleViewConst.h"
#import "PCCircleInfoView.h"
#import "PCCircle.h"
@interface ESUserFingerSetPswCtr ()<CircleViewDelegate>
{
    PCLockLabel * _alertMessageLab ;
    PCCircleInfoView * _circleInfoV;
    PCCircleView *_loCircleV;
    UIImageView * _baseV;
    UILabel * _fakeNavTitleLab;
    UIView * _baseLayerView;
}
@end

@implementation ESUserFingerSetPswCtr

- (void)viewDidLoad {
    [super viewDidLoad];
    self.myTitle = @"设置解锁手势";
    if (self.gestureType ==1) {
        [self createInputGestureView];
    }else{
        [self createSetGestureView];
    }
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    // 禁用返回手势
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    // 开启返回手势
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    }
}
-(void)createInputGestureView{
    _fakeNavTitleLab.text = @"输入解锁手势";
    _baseV = [[UIImageView alloc]initWithFrame:self.view.bounds];
    _baseV.userInteractionEnabled = YES;
    [self.view addSubview:_baseV];
    _baseV.image = Image(@"gesture_login");
    
    _loCircleV = [[PCCircleView alloc]initWithType:CircleViewTypeLogin clip:YES arrow:NO withCenterFloat:0];
    _loCircleV.delegate = self;
    [_baseV addSubview:_loCircleV];
    
    _alertMessageLab = [[PCLockLabel alloc] init];
    _alertMessageLab.frame = CGRectMake(0, 0, IPHONE_WIDTH, 14);
    _alertMessageLab.center = CGPointMake(IPHONE_WIDTH/2, CGRectGetMinY(_loCircleV.frame) - 30);
    [_baseV addSubview:_alertMessageLab];
}
-(void)createSetGestureView{
    [PCCircleViewConst saveGesture:nil Key:gestureOneSaveKey];
    _baseLayerView = [[UIView alloc]initWithFrame:Frame(0, kTopHeight, IPHONE_WIDTH, IPHONE_HEIGHT-kTopHeight-KTabbarSafeBottomMargin)];
    _baseLayerView.backgroundColor = [UIColor normalColor:UIColorWhite darkColor:UIColorBlack];
    [self.view addSubview:_baseLayerView];
    
    _fakeNavTitleLab.text = @"设置解锁手势";
    self.view.backgroundColor = [UIColor colorWithHue:0.61 saturation:0.01 brightness:0.96 alpha:1];
    _loCircleV = [[PCCircleView alloc]initWithType:CircleViewTypeSetting clip:YES arrow:NO withCenterFloat:0];
    _loCircleV.delegate = self;
    [_baseLayerView addSubview:_loCircleV];
    
    [_loCircleV setType:CircleViewTypeSetting];
    _alertMessageLab = [[PCLockLabel alloc] init];
    _alertMessageLab.frame = CGRectMake(0, 0, IPHONE_WIDTH, 14);
    _alertMessageLab.center = CGPointMake(IPHONE_WIDTH/2, CGRectGetMinY(_loCircleV.frame) - 30);
    [_baseLayerView addSubview:_alertMessageLab];
    [_alertMessageLab showNormalMsg:@"绘制解锁图案"];
    
    _circleInfoV = [[PCCircleInfoView alloc] init];
    _circleInfoV.frame = CGRectMake(0, 0, CircleRadius * 2 * 0.6, CircleRadius * 2 * 0.6);
    _circleInfoV.center = CGPointMake(kScreenW/2, CGRectGetMinY(_alertMessageLab.frame) - CGRectGetHeight(_circleInfoV.frame)/2 - 10);
    [_baseLayerView addSubview:_circleInfoV];
    
    UIButton * reDrawBtn = [UIButton new];
    [reDrawBtn setTitle:@"重新绘制" forState:0];
    [reDrawBtn setTitleColor:[UIColor normalColor:TextColor darkColor:UIColorWhite] forState:0];
    [reDrawBtn addTarget:self action:@selector(forgetBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [_baseLayerView addSubview:reDrawBtn];
    reDrawBtn.sd_layout.centerXEqualToView(_baseLayerView).bottomSpaceToView(_baseLayerView, AUTO(10));
    [reDrawBtn setupAutoSizeWithHorizontalPadding:10 buttonHeight:40];
}
- (void)circleView:(PCCircleView *)view type:(CircleViewType)type connectCirclesLessThanNeedWithGesture:(NSString *)gesture{
    NSString *gestureOne = [PCCircleViewConst getGestureWithKey:gestureOneSaveKey];
    if ([gestureOne length]) {
        [_alertMessageLab showNormalMsg:@"请重复正确手势"];
    } else {
        [_alertMessageLab showWarnMsg:@"请设置至少四个连线"];
    }
}
- (void)circleView:(PCCircleView *)view type:(CircleViewType)type didCompleteSetFirstGesture:(NSString *)gesture{
    [_alertMessageLab showNormalMsg:@"请重复手势"];
    [self infoViewSelectedSubviewsSameAsCircleView:view];
}
- (void)circleView:(PCCircleView *)view type:(CircleViewType)type didCompleteSetSecondGesture:(NSString *)gesture result:(BOOL)equal{
    if (equal) {
        NSUserDefaults * def = [NSUserDefaults standardUserDefaults];
        [def setObject:gesture forKey:@"gesturelock"];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (self.gesBackBlock) {
                self.gesBackBlock(1);
            }
            [self.navigationController popViewControllerAnimated:YES];
        });
    } else {
        [_alertMessageLab showWarnMsg:@"两次手势不匹配！"];
    }
}
#pragma mark - 让infoView对应按钮选中
- (void)infoViewSelectedSubviewsSameAsCircleView:(PCCircleView *)circleView {
    for (PCCircle *c in circleView.subviews) {
        if (c.circleState == CircleStateSelected || c.circleState == CircleStateLastOneSelected) {
            for (PCCircle *ic in _circleInfoV.subviews) {
                if (ic.tag == c.tag) {
                    [ic setCircleState:CircleStateSelected];
                }
            }
        }
    }
}
-(void)forgetGesturePsw:(UIButton*)sender{
    // 清除之前存储的密码
    [PCCircleViewConst saveGesture:nil Key:gestureOneSaveKey];
}
-(void)forgetBtnClick{
    [PCCircleViewConst saveGesture:nil Key:gestureOneSaveKey];
    [_alertMessageLab showNormalMsg:@"绘制解锁图案"];
    for (PCCircle *c in _loCircleV.subviews) {
        for (PCCircle *ic in _circleInfoV.subviews) {
            if (ic.tag != c.tag) {
                [ic setCircleState:CircleStateNormal];
            }
        }
    }
}
-(void)backClick{
    if(self.gesBackBlock){
        self.gesBackBlock(0);
    }
    [self.navigationController popViewControllerAnimated:YES];
}
-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleDefault;
}
@end
