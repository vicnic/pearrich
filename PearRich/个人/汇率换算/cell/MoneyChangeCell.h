//
//  MoneyChangeCell.h
//  PearRich
//
//  Created by Jinniu on 2020/4/2.
//  Copyright © 2020 谢黎鹏. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MoneyChangeModel.h"
NS_ASSUME_NONNULL_BEGIN
typedef void(^ChangeValueBlock)(NSString * str , NSString  * rate);
@interface MoneyChangeCell : UITableViewCell
@property(nonatomic,retain)UILabel * nameLab ;
@property(nonatomic,retain)MoneyChangeModel * model;
@property(nonatomic,copy)ChangeValueBlock changeBlock;
@end

NS_ASSUME_NONNULL_END
