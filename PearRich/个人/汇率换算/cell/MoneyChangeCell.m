//
//  MoneyChangeCell.m
//  PearRich
//
//  Created by Jinniu on 2020/4/2.
//  Copyright © 2020 谢黎鹏. All rights reserved.
//

#import "MoneyChangeCell.h"
@interface MoneyChangeCell()

@property(nonatomic,retain)UITextField * inpuTF;
@end
@implementation MoneyChangeCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self createCusUI];
    }
    return self;
}
-(void)createCusUI{
    
    self.nameLab = [UILabel labelWithTitle:@"" color:[UIColor normalColor:UIColorBlack darkColor:UIColorWhite] fontSize:14];
    [self.contentView addSubview:self.nameLab];
    _nameLab.sd_layout.leftSpaceToView(self.contentView, 15).centerYEqualToView(self.contentView).heightIs(14);
    [_nameLab setSingleLineAutoResizeWithMaxWidth:300];
    
    self.inpuTF = [UITextField new];
    _inpuTF.font = UIFontMake(18);
    _inpuTF.textColor = [UIColor normalColor:UIColorBlack darkColor:UIColorWhite];
    _inpuTF.placeholder = @"0";
    _inpuTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    _inpuTF.keyboardType = UIKeyboardTypeDecimalPad;
    [_inpuTF addTarget:self action:@selector(textFieldDidChangeValue:)
    forControlEvents:UIControlEventEditingChanged];
    _inpuTF.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:self.inpuTF];
    self.inpuTF.sd_layout.rightSpaceToView(self.contentView, 15).centerYEqualToView(self.contentView).heightIs(45).leftSpaceToView(self.nameLab, 10);
}
- (void)setModel:(MoneyChangeModel *)model{
    _model = model;
    self.nameLab.text = model.name;
    
}
- (void)textFieldDidChangeValue:(UITextField*)textField{
    if (self.changeBlock) {
        self.changeBlock(textField.text,_model.num);
    }
}
@end
