//
//  ExpressModel.h
//  PearRich
//
//  Created by Jinniu on 2020/4/2.
//  Copyright © 2020 谢黎鹏. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ExpressModel : NSObject
@property(nonatomic,copy)NSString * context, * time;
@end

NS_ASSUME_NONNULL_END
