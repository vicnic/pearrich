//
//  ExpressCheckCell.h
//  PearRich
//
//  Created by Jinniu on 2020/4/2.
//  Copyright © 2020 谢黎鹏. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ExpressModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface ExpressCheckCell : UITableViewCell
@property(nonatomic,retain)ExpressModel * model;
@end

NS_ASSUME_NONNULL_END
