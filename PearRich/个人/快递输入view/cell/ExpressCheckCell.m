//
//  ExpressCheckCell.m
//  PearRich
//
//  Created by Jinniu on 2020/4/2.
//  Copyright © 2020 谢黎鹏. All rights reserved.
//

#import "ExpressCheckCell.h"
@interface ExpressCheckCell()
@property(nonatomic,retain)UILabel * titleLab, * timeLab;
@end
@implementation ExpressCheckCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self createCusUI];
    }
    return self;
}
-(void)createCusUI{
    self.titleLab = [UILabel labelWithTitle:@"" color:UIColorBlack fontSize:14 alignment:NSTextAlignmentLeft];
    [self.contentView addSubview:self.titleLab];
    self.titleLab.sd_layout.leftSpaceToView(self.contentView, 15).topSpaceToView(self.contentView, 15).rightSpaceToView(self.contentView, 15).autoHeightRatio(0);
    
    self.timeLab = [UILabel labelWithTitle:@"" color:UIColorGrayDarken fontSize:12];
    [self.contentView addSubview:self.timeLab];
    _timeLab.sd_layout.leftEqualToView(self.titleLab).topSpaceToView(self.titleLab, 10).heightIs(14);
    [_timeLab setSingleLineAutoResizeWithMaxWidth:300];
    [self setupAutoHeightWithBottomView:_timeLab bottomMargin:5];
}
- (void)setModel:(ExpressModel *)model{
    self.titleLab.text = model.context;
    self.timeLab.text = model.time;
}
@end
