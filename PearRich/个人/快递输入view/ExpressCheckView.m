//
//  ExpressCheckView.m
//  PearRich
//
//  Created by Jinniu on 2020/4/2.
//  Copyright © 2020 谢黎鹏. All rights reserved.
//

#import "ExpressCheckView.h"
#import "ExpressCheckCell.h"
@interface ExpressCheckView()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,retain)UITableView * myTab;
@property(nonatomic,retain)UITextField * inputTF;
@property(nonatomic,retain)NSMutableArray * dataArr;
@property(nonatomic,copy)NSString * expressStatue;
@end
@implementation ExpressCheckView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor normalColor:UIColorWhite darkColor:[UIColor darkGrayColor]];
        self.cornerRadius = 5;
        [self createUI];
    }
    return self;
}
-(void)createUI{
    UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:@"查询" forState:UIControlStateNormal];
    btn.titleLabel.font = UIFontMake(14);
    btn.backgroundColor = ThemeColor;
    btn.cornerRadius = 4;
    [btn addTarget:self action:@selector(btnClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btn];
    btn.sd_layout.rightSpaceToView(self, 5).topSpaceToView(self, 5).heightIs(40).widthIs(60);
    
    self.inputTF = [UITextField new];
    _inputTF.font = UIFontMake(14);
    _inputTF.keyboardType = UIKeyboardTypeNumberPad;
    _inputTF.placeholder = @"请输入单号";
    
    [self addSubview:self.inputTF];
    _inputTF.sd_layout.leftSpaceToView(self, 15).topSpaceToView(self, 5).heightIs(40).rightSpaceToView(btn, 10);
    
    UIView * line = [UIView new];
    line.backgroundColor = [UIColor normalColor:LineColor darkColor:[UIColor lightGrayColor]];
    [self addSubview:line];
    line.sd_layout.leftEqualToView(self).rightEqualToView(self).topSpaceToView(_inputTF, 10).heightIs(1);
    
    self.dataArr = [NSMutableArray new];
    self.myTab = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.myTab.delegate =self;
    self.myTab.dataSource = self;
    self.myTab.alpha = 0;
    [self addSubview:self.myTab];
    self.myTab.sd_layout.leftEqualToView(self).rightEqualToView(self).topSpaceToView(line, 5).bottomSpaceToView(self, 10);
}
-(void)btnClick{
    if (self.inputTF.text.length == 0) {
        [QMUITips showError:@"单号不能为空"];
    }
    if (self.dataArr.count) {
        [self.dataArr removeAllObjects];
    }
    NSString * url = [NSString stringWithFormat:@"%@%@",@"https://api.m.sm.cn/rest?method=kuaidi.getdata&sc=express_cainiao&q=",self.inputTF.text];
    [QMUITips showLoadingInView:self.superview];
    [PGNetworkHelper GET:url parameters:nil cache:NO responseCache:nil success:^(id responseObject) {
        [QMUITips hideAllTips];
        if ([responseObject[@"status"] integerValue]==1) {
            self.expressStatue = responseObject[@"data"][@"status"];
            NSArray * arrm = [ExpressModel mj_objectArrayWithKeyValuesArray:responseObject[@"data"][@"messages"]];
            [self.dataArr addObjectsFromArray:arrm];
            [UIView animateWithDuration:0.35 animations:^{
                self.height = IPHONE_HEIGHT - 200;
                if (self.myTab.alpha == 0) {
                    self.myTab.alpha = 1;
                }                
            }];
            [self.myTab reloadData];
        }else{
            [QMUITips showError:@"获取失败"];
        }
    } failure:^(NSError *error) {
        [QMUITips hideAllTips];
        [QMUITips showError:@"获取失败"];
    }];
}
#pragma mark tableView代理
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArr.count;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ExpressCheckCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if(!cell){
        cell = [[ExpressCheckCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    cell.model = self.dataArr[indexPath.row];
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    ExpressModel * mo = self.dataArr[indexPath.row];
    return [tableView cellHeightForIndexPath:indexPath model:mo keyPath:@"model" cellClass:[ExpressCheckCell class] contentViewWidth:IPHONE_WIDTH];
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 45;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView * header = [UIView new];
    header.backgroundColor = [UIColor whiteColor];
    UILabel * lab = [UILabel labelWithTitle:self.expressStatue color:UIColorBlack fontSize:16];
    [header addSubview:lab];
    lab.sd_layout.centerYEqualToView(header).leftSpaceToView(header, 15).heightIs(16);
    [lab setSingleLineAutoResizeWithMaxWidth:300];
    return  header;
}
-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return nil;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}
@end
