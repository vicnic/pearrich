//
//  GoodsNameMgrModel.h
//  PearRich
//
//  Created by Jinniu on 2020/3/18.
//  Copyright © 2020 谢黎鹏. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BGFMDB/BGFMDB.h>
NS_ASSUME_NONNULL_BEGIN

@interface GoodsNameMgrModel : NSObject
@property(nonatomic,copy)NSString * name;
@end

NS_ASSUME_NONNULL_END
