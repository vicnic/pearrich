//
//  ReuseFile.m
//  PropertyManager
//
//  Created by Jinniu on 2019/4/2.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import "ReuseFile.h"
@implementation ReuseFile
+(void)getOcrMainUrl:(_Nullable RequestReturnBlock)block{
    NSString * url = @"https://i.rcuts.com/update.php?id=187";
    [PGNetworkHelper GET:url parameters:nil cache:NO responseCache:nil success:^(id responseObject) {
        id obj = responseObject[@"api"];
        if (![obj isKindOfClass:[NSNull class]]) {
            [DLUserDefaultModel userDefaultsModel].ocrApi = (NSString*)obj;
            if(block){
                block(responseObject);
            }
        }else{
            [QMUITips showError:@"文字识别暂不可用"];
        }
    } failure:^(NSError *error) {
        [QMUITips hideAllTips];
    }];
}

@end
