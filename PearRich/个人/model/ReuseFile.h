//
//  ReuseFile.h
//  PropertyManager
//
//  Created by Jinniu on 2019/4/2.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
typedef void (^RequestReturnBlock)(id response);
@interface ReuseFile : NSObject

+(void)getOcrMainUrl:(_Nullable RequestReturnBlock)block;

@end

NS_ASSUME_NONNULL_END
