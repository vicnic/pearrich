//
//  PRRedBookColCell.h
//  PearRich
//
//  Created by apple on 2020/12/25.
//  Copyright © 2020 谢黎鹏. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PRRedBookColModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface PRRedBookColCell : UICollectionViewCell
@property(nonatomic,retain)PRRedBookColModel * model;
@end

NS_ASSUME_NONNULL_END
