//
//  PRRedBookColCell.m
//  PearRich
//
//  Created by apple on 2020/12/25.
//  Copyright © 2020 谢黎鹏. All rights reserved.
//

#import "PRRedBookColCell.h"
@interface PRRedBookColCell()
@property(nonatomic,retain)UIImageView * imgV, * arrowView;
@end
@implementation PRRedBookColCell
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}
-(void)createUI{
    self.imgV = [UIImageView new];
    [self.contentView addSubview:self.imgV];
    self.imgV.sd_layout.spaceToSuperView(UIEdgeInsetsZero);
    
    self.arrowView = [UIImageView new];
    self.arrowView.image = Image(@"打钩_normal");
    [self.contentView addSubview:self.arrowView];
    self.arrowView.sd_layout.rightSpaceToView(self.contentView, 6).topSpaceToView(self.contentView, 6).widthIs(16).heightEqualToWidth();
}
- (void)setModel:(PRRedBookColModel *)model{
    [self.imgV sd_setImageWithURL:URL(model.url) placeholderImage:Image(@"holderImg")];
    if (model.isChose) {
        self.arrowView.image = Image(@"打钩_sel");
    }else{
        self.arrowView.image = Image(@"打钩_normal");
    }
}
@end
