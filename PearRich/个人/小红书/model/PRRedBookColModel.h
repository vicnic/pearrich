//
//  PRRedBookColModel.h
//  PearRich
//
//  Created by apple on 2020/12/25.
//  Copyright © 2020 谢黎鹏. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PRRedBookColModel : NSObject
@property(nonatomic,copy)NSString * url;
@property(nonatomic,assign)BOOL isChose;
@end

NS_ASSUME_NONNULL_END
