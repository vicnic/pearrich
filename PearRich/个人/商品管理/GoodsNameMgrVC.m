//
//  GoodsNameMgrVC.m
//  PearRich
//
//  Created by Jinniu on 2020/3/18.
//  Copyright © 2020 谢黎鹏. All rights reserved.
//

#import "GoodsNameMgrVC.h"
#import "GoodsNameMgrCell.h"
@interface GoodsNameMgrVC ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,retain)UITableView * myTab;
@property(nonatomic,retain)NSMutableArray * dataArr;
@end

@implementation GoodsNameMgrVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.myTitle = @"商品管理";
    self.dataArr = [NSMutableArray new];
    self.myTab = [[UITableView alloc]initWithFrame:Frame(0, kTopHeight, IPHONE_WIDTH, IPHONE_HEIGHT - kTopHeight - KTabbarSafeBottomMargin) style:UITableViewStyleGrouped];
    _myTab.delegate = self;
    _myTab.dataSource = self;
    _myTab.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.myTab];
    self.rightBtnName = @"删除全部";
    self.rightBtnFontSize = 14;
    self.rightBtnNameColor = [UIColor normalColor:UIColorBlack darkColor:UIColorWhite];
    [self getData];
}
-(void)getData{
    NSArray * arr = [GoodsNameMgrModel bg_findAll:GoodsNameTab];
    [self.dataArr addObjectsFromArray:arr];
    [self.myTab reloadData];
    [Mytools NeedResetNoViewWithTable:self.myTab andArr:self.dataArr];
}
- (void)rightBtnClick{
    [QMUITips showLoadingInView:self.view];
    [GoodsNameMgrModel bg_clearAsync:GoodsNameTab complete:^(BOOL isSuccess) {
        [QMUITips showSucceed:@"删除完成"];
        [self.dataArr removeAllObjects];
        [self.myTab reloadData];
    }];
}
#pragma mark tableView代理
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArr.count;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    GoodsNameMgrCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if(!cell){
        cell = [[GoodsNameMgrCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    GoodsNameMgrModel * mo = self.dataArr[indexPath.row];
    cell.model = mo;
    cell.reloadBlock = ^{
        [self.dataArr removeAllObjects];
        [self getData];
    };
    if (self.dataArr.count == 1) {
        cell.bgView.cornerRadius = 5;
    }else{
        if (indexPath.row == 0) {
            UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:Frame(0, 0, IPHONE_WIDTH-30, 45) byRoundingCorners:UIRectCornerTopLeft|UIRectCornerTopRight cornerRadii:CGSizeMake(5, 5)];
            CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
            maskLayer.frame = cell.bgView.bounds;
            maskLayer.path = maskPath.CGPath;
            cell.bgView.layer.mask = maskLayer;
        }else if (indexPath.row == self.dataArr.count - 1){
            UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:Frame(0, 0, IPHONE_WIDTH-30, 45) byRoundingCorners:UIRectCornerBottomLeft|UIRectCornerBottomRight cornerRadii:CGSizeMake(5, 5)];
            CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
            maskLayer.frame = cell.bgView.bounds;
            maskLayer.path = maskPath.CGPath;
            cell.bgView.layer.mask = maskLayer;
        }
    }    
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return  nil;
}
-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return nil;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}
@end
