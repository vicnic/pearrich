//
//  GoodsNameMgrCell.h
//  PearRich
//
//  Created by Jinniu on 2020/3/18.
//  Copyright © 2020 谢黎鹏. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoodsNameMgrModel.h"
NS_ASSUME_NONNULL_BEGIN
typedef void(^ReloadBlock)(void);
@interface GoodsNameMgrCell : UITableViewCell
@property(nonatomic,retain)GoodsNameMgrModel * model;
@property(nonatomic,retain)UIView * bgView;
@property(nonatomic,copy)ReloadBlock reloadBlock;
@end

NS_ASSUME_NONNULL_END
