//
//  GoodsNameMgrCell.m
//  PearRich
//
//  Created by Jinniu on 2020/3/18.
//  Copyright © 2020 谢黎鹏. All rights reserved.
//

#import "GoodsNameMgrCell.h"
@interface GoodsNameMgrCell()
@property(nonatomic,retain)UILabel * nameLab;
@end
@implementation GoodsNameMgrCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self createCusUI];
    }
    return self;
}
-(void)createCusUI{
    self.bgView = [UIView new];
    self.bgView.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:self.bgView];
    self.bgView.sd_layout.spaceToSuperView(UIEdgeInsetsMake(0, 15, 0, 15));
    
    self.nameLab = [UILabel labelWithTitle:@"" color:[UIColor blackColor] fontSize:14];
    [self.bgView addSubview:self.nameLab];
    _nameLab.sd_layout.leftSpaceToView(self.bgView, 10).centerYEqualToView(self.bgView).heightIs(14);
    [_nameLab setSingleLineAutoResizeWithMaxWidth:300];
    
    UIView * line = [UIView new];
    line.backgroundColor = LineColor;
    [self.bgView addSubview:line];
    line.sd_layout.leftEqualToView(self.bgView).rightEqualToView(self.bgView).bottomEqualToView(self.bgView).heightIs(0.7);
    
    UIButton * delBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [delBtn setImage:Image(@"close") forState:UIControlStateNormal];
    [delBtn addTarget:self action:@selector(delBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.bgView addSubview:delBtn];
    delBtn.sd_layout.rightSpaceToView(self.bgView, 10).topEqualToView(self.bgView).bottomEqualToView(self.bgView).widthEqualToHeight();
}
-(void)delBtnClick{
    NSString* where = [NSString stringWithFormat:@"where %@=%@",bg_sqlKey(bg_primaryKey),_model.bg_id];
    [GoodsNameMgrModel bg_delete:GoodsNameTab where:where];
    if (self.reloadBlock) {
        self.reloadBlock();
    }
}
- (void)setModel:(GoodsNameMgrModel *)model{
    _model = model;
    _nameLab.text = model.name;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
