//
//  AppDelegate.m
//  PearRich
//
//  Created by 谢黎鹏 on 2020/1/11.
//  Copyright © 2020年 谢黎鹏. All rights reserved.
//

#import "AppDelegate.h"
#import "NewTabbarVC.h"
#import "IQKeyboardManager.h"
#import <YZAuthID/YZAuthID.h>
#import "GestureLoginVC.h"
@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    IQKeyboardManager *manager = [IQKeyboardManager sharedManager];
    manager.enable = YES;//控制整个功能是否启用。
    manager.shouldResignOnTouchOutside = YES;//控制点击背景是否收起键盘
    manager.shouldShowToolbarPlaceholder = YES;//中间位置是否显示占位文字
    manager.placeholderFont = [UIFont boldSystemFontOfSize:16];//设置占位文字的字体
    manager.enableAutoToolbar = YES;//控制是否显示键盘上的工具条。
        
    [self launchApp];
    return YES;
}

-(void)launchApp{
    NSString * flag = iPhoneX?[DLUserDefaultModel userDefaultsModel].faceIdentify : [DLUserDefaultModel userDefaultsModel].thumbIdentify;
    if ([flag isEqualToString:@"YES"]) {
        [self authIDVerify];
    }else if ([[DLUserDefaultModel userDefaultsModel].gestureIdentify isEqualToString:@"YES"]){
        GestureLoginVC * vc = [GestureLoginVC new];
        self.window.rootViewController = vc;
    }else{
        NewTabbarVC * tab = [NewTabbarVC new];
        self.window.rootViewController = tab;
    }
}
-(void)authIDVerify{
    [[YZAuthID alloc] yz_showAuthIDWithDescribe:nil block:^(YZAuthIDState state, NSError *error) {
        if (state == YZAuthIDStateNotSupport) {
            [QMUITips showError:@"对不起，当前设备不支持指纹/面部ID"];
        } else if(state == YZAuthIDStateFail) {
            [QMUITips showError:@"指纹/面部ID不正确，认证失败"];
        } else if(state == YZAuthIDStateTouchIDLockout) {
            [QMUITips showError:@"多次错误，指纹/面部ID已被锁定，请到手机解锁界面输入密码"];
        } else if(state == YZAuthIDStateUserCancel){
            [self authIDVerify];
        } else if (state == YZAuthIDStateSuccess) {
            NewTabbarVC * tab = [NewTabbarVC new];
            self.window.rootViewController = tab;
        }
    }];
}
@end
