//
//  DLUserDefaultModel.m
//  SABusiness
//
//  Created by 谢黎鹏 on 2019/12/15.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import "DLUserDefaultModel.h"

@implementation DLUserDefaultModel
@dynamic isShowLogoSW;
@dynamic faceIdentify;
@dynamic thumbIdentify;
@dynamic gestureIdentify;
@dynamic ocrApi;
@dynamic isMeSayFirst;
@dynamic chaterName;
@dynamic rateReqDate;

@dynamic CNY;
@dynamic USD;
@dynamic JPY;
@dynamic GBP;
@dynamic HKD;
@dynamic EUR;
@dynamic KRW;
@dynamic THB;
@dynamic TWD;
- (NSDictionary *)setupDefaultValues{
    return @{
             @"isShowLogoSW":@"",
             @"faceIdentify":@"NO",
             @"thumbIdentify":@"NO",
             @"gestureIdentify":@"NO",
             @"ocrApi":@"",
             @"isMeSayFirst":@"NO",
             @"chaterName":@"",
             @"CNY":@"",
             @"USD":@"",
             @"JPY":@"",
             @"GBP":@"",
             @"HKD":@"",
             @"EUR":@"",
             @"KRW":@"",
             @"THB":@"",
             @"TWD":@"",
             };
}
@end
