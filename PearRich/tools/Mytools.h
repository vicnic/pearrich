//
//  Mytools.h
//  7UGame
//
//  Created by 111 on 2017/2/14.
//  Copyright © 2017年 111. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface Mytools : NSObject
//用于MD5加密的封装方法
+(NSString *)getMD5code:(NSString *)pswStr;

//获取当前时间戳
+(NSString*)getCurrentTimestamp;

//字符串转日期
+(NSDate*)dateStrToDate:(NSString *)dateStr;

//日期转字符串
+(NSString *)dateToDateStr:(NSDate*)date;
+(void)saveImageToLocal:(UIImage *)img;
//返回当天的日期
+(NSDate *)getCurrentDate;
//获取当前视图控制器
+(UIViewController*)currentViewController;
//日期大小比较yyyy-MM-dd
+(int)compareOneDay:(NSDate *)oneDay withAnotherDay:(NSDate *)anotherDay;
//判断是不是浮点数
+(BOOL)isPureFloat:(NSString*)string;
//url编码
+(NSString*)encodeString:(NSString*)unencodedString;

//时间戳转时间
+ (NSString *)timeStampTotime:(NSString *)timeString;

//json格式字符串转字典
+ (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString;

//字典转json格式字符串
+ (NSString*)dictionaryToJson:(NSDictionary *)dic;

//数组转字符串
+ (NSString*)ArrtionaryToJson:(NSMutableArray *)arr;

+(NSInteger)howManyDaysInThisYear:(NSInteger)year withMonth:(NSInteger)month;
//判断是不是整数
+(BOOL)isPureInt:(NSString*)string;

+(NSString*)returnDecimalNumStr:(NSString*)numStr;//解决json解析精度丢失的问题
+(void)NeedResetNoViewWithTable:(UITableView*)tab andArr:(NSMutableArray*)arr;
//从字符串中找出所有的url以数组返回
+(NSArray *)filterUrlWithString:(NSString*)str;
@end
