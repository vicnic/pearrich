//
//  UIColor+Hex.h
//  yunFanPiaoWu
//
//  Created by 艾腾软件 on 26/8/16.
//  Copyright © 2016年 艾腾. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Hex)

+ (UIColor *)colorWithHexString:(NSString *)color;

+ (UIColor *)colorWithHexString:(NSString *)color alpha:(CGFloat)alpha;

+(UIColor*)normalColor:(UIColor*)nc darkColor:(UIColor*)dc;
@end
