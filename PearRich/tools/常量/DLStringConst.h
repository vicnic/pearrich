//
//  DLStringConst.h
//  PearRich
//
//  Created by Jinniu on 2020/3/25.
//  Copyright © 2020 谢黎鹏. All rights reserved.
//

#ifndef DLStringConst_h
#define DLStringConst_h

static NSString *const QrCodeLogoFilePath = @"QrCodeLogoFilePath";
static NSString *const ChatLogoFilePath = @"ChatLogoFilePath";
#endif /* DLStringConst_h */
