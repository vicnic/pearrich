//
//  BXNavicationController.m
//  SABusiness
//
//  Created by Jinniu on 2019/12/12.
//  Copyright © 2019 Jinniu. All rights reserved.
//

#import "BXNavicationController.h"

@interface BXNavicationController ()<UINavigationControllerDelegate>

@end

@implementation BXNavicationController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.popDelegate = self.interactivePopGestureRecognizer.delegate;
    self.delegate = self;
}
-(void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated{
    if (self.childViewControllers.count>0) {
        viewController.hidesBottomBarWhenPushed = YES;
        
        // 就有滑动返回功能
        self.interactivePopGestureRecognizer.delegate = nil;
    }
    [super pushViewController:viewController animated:animated];
}
@end
