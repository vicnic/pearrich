//
//  NewTabbarVC.m
//  IDCardManager
//
//  Created by 金牛 on 2017/9/28.
//  Copyright © 2017年 zou. All rights reserved.
//

#import "NewTabbarVC.h"
#import "PRMainViewCtr.h"
#import "MySearchCtr.h"
#import "PersonCtr.h"
#import "BXNavicationController.h"
@interface NewTabbarVC ()

@end

@implementation NewTabbarVC
- (void)viewDidLoad {
    [super viewDidLoad];
    self.tabBar.tintColor = ThemeColor;//ios 13 pop bug
//    [UITabBar appearance].translucent = NO;
    //首页
    PRMainViewCtr * vc = [[PRMainViewCtr alloc]init];
    vc.tabBarItem = [[UITabBarItem alloc]initWithTitle:@"首页" image:Image(@"球场") selectedImage:[self backImage:Image(@"球场-绿色")]];
    BXNavicationController * navVC = [[BXNavicationController alloc]initWithRootViewController:vc];
    //
    MySearchCtr * repay = [[MySearchCtr alloc]init];
    repay.tabBarItem = [[UITabBarItem alloc]initWithTitle:@"搜索" image:Image(@"社区") selectedImage:[self backImage:Image(@"社区-绿色")]];
    BXNavicationController * repayNav = [[BXNavicationController alloc]initWithRootViewController:repay];
    
    //个人
    PersonCtr * person  = [PersonCtr new];
    person.tabBarItem = [[UITabBarItem alloc]initWithTitle:@"个人" image:Image(@"个人") selectedImage:[self backImage:Image(@"个人-绿色")]];
    BXNavicationController * personNav = [[BXNavicationController alloc]initWithRootViewController:person];
    [[UITabBarItem appearance]setTitleTextAttributes:@{NSForegroundColorAttributeName:ThemeColor} forState:UIControlStateSelected];

    self.viewControllers = @[navVC,repayNav,personNav];
    self.selectedIndex = 0;
}


-(UIImage*)backImage:(UIImage*)image{
    return  [image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
