//
//  SuperViewController.m
//
//
//  Created by 111 on 2017/4/26.
//   Copyright © 2019年 jinniu All rights reserved.
//

#import "SuperViewController.h"

@interface SuperViewController ()
@property(nonatomic,retain)UILabel * titleLab;
@property(nonatomic,retain)UIView *bottomLine;
@property(nonatomic,retain)UIButton * rightBtn;
@property(nonatomic,retain)UIButton * backBtn;
@end

@implementation SuperViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = YES;
    self.view.backgroundColor = [UIColor whiteColor];
    [self createNavBar];
}
-(void)createNavBar{
    if (_navView == nil) {
        _navView = [[UIView alloc]initWithFrame:Frame(0, 0, IPHONE_WIDTH, self.navBarHeight==0?kTopHeight:self.navBarHeight)];
        _navView.backgroundColor=self.navBgColor==nil?[UIColor normalColor:[UIColor whiteColor] darkColor:[UIColor blackColor]]:self.navBgColor;
        [self.view addSubview:_navView];
        self.titleLab = [[UILabel alloc]initWithFrame:Frame(IPHONE_WIDTH/2-AUTO(100), kStatusBarHeight, AUTO(200), kNavBarHeight)];
        _titleLab.text = self.myTitle;
        _titleLab.textAlignment = NSTextAlignmentCenter;
        _titleLab.textColor = [UIColor normalColor:[UIColor blackColor] darkColor:[UIColor whiteColor]];
        [_navView addSubview:self.titleLab];
        if (self.navigationController.viewControllers.count>1) {
            self.backBtn = [[UIButton alloc]initWithFrame:Frame(0, kStatusBarHeight, kNavBarHeight, kNavBarHeight)];
            [_backBtn setImage:Image(@"fanhui") forState:UIControlStateNormal];
            [_backBtn addTarget:self action:@selector(backClick) forControlEvents:UIControlEventTouchUpInside];
            [_navView addSubview:self.backBtn];
        }
        
        //line
        self.bottomLine=[UIView new];
        _bottomLine.backgroundColor = [UIColor normalColor:[UIColor colorWithRed:197/255.0 green:197/255.0 blue:197/255.0 alpha:1] darkColor:[UIColor darkGrayColor]];
        [_navView addSubview:_bottomLine];
        _bottomLine.sd_layout.widthIs(IPHONE_WIDTH).heightIs(0.6).bottomEqualToView(_navView);
    }
}
-(void)setRightBtnName:(NSString *)rightBtnName{
    if (_rightBtn ==nil) {
        _rightBtnName = rightBtnName;
        [self createRightButton];
    }else{
        [_rightBtn setTitle:rightBtnName forState:UIControlStateNormal];
    }
//    if (_rightBtnImage!=nil) {
//        _rightBtn.sd_layout.widthIs(80);
//    }
}
-(void)setRightBtnImage:(UIImage *)rightBtnImage{
    if (rightBtnImage != nil) {
        _rightBtnImage = rightBtnImage;
    }else{
        _rightBtnImage = [UIImage imageNamed:@""];
    }
    if (_rightBtn ==nil) {
        [self createRightButton];
        //如果是图片就改变约束
        _rightBtn.sd_layout.rightSpaceToView(_navView, 10).topSpaceToView(_navView, kStatusBarHeight+kNavBarHeight/2.f-15).heightIs(30).widthEqualToHeight();
    }else{
        [_rightBtn setImage:rightBtnImage forState:UIControlStateNormal];
    }
//    if (_rightBtnName.length>0) {
//        _rightBtn.sd_layout.widthIs(80);
//    }
}
-(void)createRightButton{
    self.rightBtn = [UIButton new];
    [_rightBtn addTarget:self action:@selector(rightBtnClick) forControlEvents:UIControlEventTouchUpInside];
    if (_rightBtnImage!=nil) {
        [_rightBtn setImage:_rightBtnImage forState:UIControlStateNormal];
    }
    if (_rightBtnName.length>0) {
        [_rightBtn setTitle:_rightBtnName forState:UIControlStateNormal];
    }
    [_navView addSubview:self.rightBtn];
    _rightBtn.sd_layout.rightSpaceToView(_navView, 10).topSpaceToView(_navView, kStatusBarHeight);
    [_rightBtn setupAutoSizeWithHorizontalPadding:5 buttonHeight:kNavBarHeight];
}
-(void)setRightBtnNameColor:(UIColor *)rightBtnNameColor{
    if (rightBtnNameColor !=nil) {
        _rightBtnNameColor = rightBtnNameColor;
    }else{
        _rightBtnNameColor = [UIColor whiteColor];
    }
    [self.rightBtn setTitleColor:self.rightBtnNameColor forState:0];
}
-(void)setIsHideBottomLine:(BOOL)isHideBottomLine{
    if(isHideBottomLine==YES){
        self.bottomLine.hidden = YES;
    }else{
        self.bottomLine.hidden = NO;
    }
}
-(void)setRightBtnFontSize:(CGFloat)rightBtnFontSize{
    if (rightBtnFontSize>0) {
        _rightBtnFontSize = rightBtnFontSize;
    }else{
        _rightBtnFontSize = 17;
    }
    self.rightBtn.titleLabel.font = [UIFont systemFontOfSize:self.rightBtnFontSize];
}
-(void)setNavBarHeight:(CGFloat)navBarHeight{
    if (navBarHeight>0) {
        _navBarHeight = navBarHeight;
    }else{
        _navBarHeight = kTopHeight;
    }
    _navView.height = _navBarHeight;
}
-(void)setIsHideNavBar:(BOOL)isHideNavBar{
    _isHideNavBar = isHideNavBar;
    _navView.hidden = isHideNavBar;
}
-(void)setIsHideLeftBtn:(BOOL)isHideLeftBtn{
    if (isHideLeftBtn == YES) {
        [self.backBtn removeFromSuperview];
        self.backBtn = nil;
    }
}
-(void)setMyTitle:(NSString *)myTitle{
    if (myTitle.length>0) {
        _myTitle = myTitle;
    }else{
        _myTitle = @"";
    }
    _titleLab.text = self.myTitle;
}
-(void)setLeftBtnImage:(NSString *)leftBtnImage{
    if (leftBtnImage) {
        [_backBtn setImage:Image(leftBtnImage) forState:UIControlStateNormal];
    }
}
-(void)setNavTitleFontSize:(CGFloat)navTitleFontSize{
    if (navTitleFontSize>0) {
        _navTitleFontSize = navTitleFontSize;
    }else{
        _navTitleFontSize = 17;
    }
    _titleLab.font = [UIFont systemFontOfSize:self.navTitleFontSize];
}
-(void)setNavBgColor:(UIColor *)navBgColor{
    if (navBgColor !=nil) {
        _navBgColor = navBgColor;
        if (navBgColor == [UIColor whiteColor]||self.isSettingMyTitleColor) {//如果用户设置了导航条背景色那就判断下
            _titleLab.textColor = [UIColor blackColor];
        }else{
            _titleLab.textColor = [UIColor whiteColor];
        }
    }else{
        _navBgColor = ThemeColor;//这里自己用
    }
    self.navView.backgroundColor = _navBgColor;
    [self setNeedsStatusBarAppearanceUpdate];//刷新状态栏重新调用preferredStatusBarStyle
}
-(void)setMyTitleColor:(UIColor *)myTitleColor{
    if (myTitleColor!=nil) {
        _titleLab.textColor = myTitleColor;
    }else{
        _titleLab.textColor = [UIColor blackColor];
    }
}
-(UIStatusBarStyle)preferredStatusBarStyle{
    if (@available(iOS 13.0,*)) {
        if (UITraitCollection.currentTraitCollection.userInterfaceStyle == UIUserInterfaceStyleDark) {
            if (self.navView !=nil) {
                BOOL isWhite = [self isTheSameColor2:self.navView.backgroundColor anotherColor:[UIColor whiteColor]];
                if (isWhite) {
                    return UIStatusBarStyleDarkContent;
                }else{
                    return  UIStatusBarStyleLightContent;
                }
            }else{
                return UIStatusBarStyleDefault;
            }
        }else{
            return [self getStatueStyle];
        }
    }else{
        return [self getStatueStyle];
    }
}
-(UIStatusBarStyle)getStatueStyle{
    if (self.navView !=nil) {
        BOOL isWhite = [self isTheSameColor2:self.navView.backgroundColor anotherColor:[UIColor whiteColor]];
        if (isWhite) {
            return UIStatusBarStyleDefault;
        }else{
            return  UIStatusBarStyleLightContent;
        }
    }else{
        return UIStatusBarStyleDefault;
    }
}
- (BOOL) isTheSameColor2:(UIColor*)color1 anotherColor:(UIColor*)color2{
    return  CGColorEqualToColor(color1.CGColor, color2.CGColor);
}
#pragma mark 点击事件
-(void)backClick{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)rightBtnClick{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end

